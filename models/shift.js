const mongoose = require('mongoose')


const shiftSchema = new mongoose.Schema({
    nameShift:{
        type:String,
        require: true
    },
    // expected pasok
    expectedStartTime:{
        type: String,
      
    },
    // expected awasan
    expectedEndTime:{
        type:String,
       
    },
    // disable enable SHIFT
    isActive:{
        type:Boolean,
        default:true
    }
})


module.exports = mongoose.model("Shift", shiftSchema);