const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    isAdmin:{
        type:Boolean,
        default: false
    },

    firstName: {
        type: String,
        uppercase: true,
        // required: [true, "Firstname is required"]
    },
    middleName: {
        type: String,
        uppercase:true
    },
    lastName: {
        type: String,
        uppercase:true,
        // required: [true, "Firstname is required"]
    },
    birthDate:{
        type: Date,
    },
    civilStatus:{
        type:String
    },
    contacts:{
        mobileNum:{
            type:String
        },
        email:{
            type: String,
            trim: true,
            lowercase: true,
            // unique: true,
            // required: [true, "Email required"]
        }
    },
    password:{
        type:String,
        minLength: 8,
        default: "i$0ft2o2tw0"
        // required: [true, "Password is required"]
    },
    companyProfile:{
        // if deptId populate department name 
        departmentId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Department'
        },
        jobPositionId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'JobPosition'
        }
      
    },   
    // INTERN , PROBATIONARY, REGULAR 
    employmentStatus:{
        type: String
    },
    paySlips:[{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'PaySlip'
    }],
    sssNum:{
        type:String,
    },
    tinNum:{
        type:String,
    },
    philHealthNum:{
        type:String,
    },
    address:{
        province:{
            type:String,
            uppercase:true,
            // required: [true, "Province is required"]
        },
        municipality: {
            type: String,
            uppercase:true,
            // required: [true, "Municipality is required"]
        },
        barangay:{
            type: String,
            uppercase:true,
            // required: [true, "Barangay is required"]
        },
        houseNumStreet: {
            type:String,
            uppercase:true,
            // required: [true, "Put n/a if not applicable"]
        },
        zipcode:{
            type: String,
            // required: [true, "Zip Code is required"]
        }
    },
    // REFERENCE SA ATTENDANCE Model para ma view ni user ang attendance history nya
    attendance:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Attendance'
    }],
    // upon creation of a new User if status is regular 
    leaveCredits:{
        sickLeave:{
            type:Number,
            default: 10
        },
        vacationLeave:{
            type:Number,
            default:10
        }
    },
    credentials:[{
        //take value name from the frontend? // NBI Clearance, Police Clearance and etc..
        name:{
            type:String,
        },
        // File Upload
        imagePath:{
            type:String
        }
    }],
    notifications:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Notification'
    },
    isActive:{
        type:Boolean,
        default:true
    },
    isSuspended:{
        type:Boolean,
        default:false
    },
    //This will be used as basis to count how many days have lapsed since the User's suspension.
    suspensionStartDate:{
        type:Date
    }
})

userSchema.set('timestamps', true);
module.exports = mongoose.model("User", userSchema);