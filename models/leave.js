const mongoose = require('mongoose')

const leaveSchema = new mongoose.Schema({
    
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    datesCovered:{
        startDate:{
            type: Date,
            default: new Date()
        },
        endDate:{
            type: Date,
            default: new Date()
        }
    },
    typeOfLeave:{
        type:String,
    },
    status:{
        type: String,
        default: "pending"
    },
    message:{
        type:String
    }
})


module.exports = mongoose.model('Leave',leaveSchema);
