const mongoose = require ('mongoose');


const notificationSchema = new mongoose.Schema({
    //request password, applied leave, 
    //NAME OF NOTIFICATION
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    name:{
        type:String
    },
    notificationDate:{
        type:Date,
        default: new Date()
    },
    // if notification is approved = GREEN CHECK, else GRAY CHECK
    isApproved:{
        type:Boolean,
        default:false
    }
})



module.exports = mongoose.model('Notification', notificationSchema);

