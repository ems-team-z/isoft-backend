const mongoose = require('mongoose')


const Schema = mongoose.Schema;


const attendanceSchema = new Schema({
    department: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Department',
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    shift: {
        type: String,
        required: true
    },
    timeIn: {
        type: String,
        required: true
    },
    timeOut: {
        type: String,
        default: null
    },
    status:{
        type:String,
        default: null
    },
    breakConsumed: {
        type: Number,
        default: 0
    },
    overTime:{
        type:Number,
        default: 0
    },
    isOvertime:{
        type: Boolean,
        default: false
    },
    date: {
        type: String,
        required: true
    },
})

module.exports = mongoose.model("Attendance", attendanceSchema);





// const attendanceSchema = new mongoose.Schema([{
//     department:{
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Department'
//     },
//     // reference sa USER para makita ni admin kaninong ATTENDANCE TO
//     userId:{
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'User'
//     },
//     // reference sa SHIFT
//     shift:{
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Shift'
//     },
//     clockIn:{
//         type: Date, default: new Date()
//     },
//     startBreak:{
//         type: String
//     },
//     endBreak:{
//         type: String
//     },
//     clockOut:{
//         type:Date, default: new Date()
//     },
//     // remarks underTime, absent,late
//     status:{
//         type:String,
//         require: true
//     },
//     // NUMBER of OT hours para magamit sa computation sa paySlip
//     overTime:{
//         type:Number,
//         default: 0
//     },
//     // kapag approve ni admin ang OT true dapat to
//     isOvertime:{
//         type: Boolean,
//         default: false
//     }
// }])


// attendanceSchema.set('timestamps', true);
// module.exports = mongoose.model("Attendance", attendanceSchema);