const mongoose = require('mongoose');



const departmentSchema = new mongoose.Schema ({
    name:{
        type:String,
        required: [true, "Password is required"]
    },
    // Department can be abolished or restored
    isActive:{
        type: Boolean,
        default:true
    },
    positions:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'JobPosition',
    }],
    activeEmployeeCount:{
        type:Number,
        default:0
    }
})


const jobPositionSchema = new mongoose.Schema ({

        name:{
            type: String,
             required: [true, "jobTitle is required"],
             uppercase:true
        },
        salary:{
            type:Number,
            required: [true, "Salary is required"]
        },
        incentives:[{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Incentive',
        }],
        deductions:[{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Deduction',
        }],
        // abolish job position 
        isActive:{
            type:Boolean,
            default:true
        },
        // // para makita department sa user profile
        // departmentId:{
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref:'Department',
        //     required:true
        // }
    
})


const deductionSchema = new mongoose.Schema ({
    deductionName:{
        type:String,
        required: [true, "name is required"]
    },
    // let s = salary, r=rate
    // formula for fixed amount (c*1) constant = desired amount * 1
    deductionFormula:{
        type:String,
        required:true
    },
    deductionAmount:{
        type: Number,
        default:0
    },
    // deduction can be abolished or restored
    isActive:{
        type: Boolean,
        default:true
    }
})


const incentiveSchema = new mongoose.Schema ({
    incentiveName:{
        type:String,
        required: [true, "name is required"]
    },
    // let s = salary, r=rate
    incentiveFormula:{
        type:String,
        required:true
    },
    incentiveAmount:{
        type: Number,
        default:0
    },
    // Incentive can be abolished or restored
    isActive:{
        type: Boolean,
        default:true
    }
})







incentiveSchema.set('timestamps', true);
deductionSchema.set('timestamps', true);
departmentSchema.set('timestamps', true);
jobPositionSchema.set('timestamps', true);

const Department =  mongoose.model('Department',departmentSchema);
const JobPosition =  mongoose.model('JobPosition',jobPositionSchema);
const Deduction =mongoose.model('Deduction',deductionSchema);
const Incentive= mongoose.model('Incentive',incentiveSchema);

module.exports= { Department, JobPosition, Deduction, Incentive };