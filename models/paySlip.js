const mongoose = require('mongoose');


const paySlipSchema = new mongoose.Schema({
    userId:{
        // must populate with user's name, department, jobTitle,
        type:mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    // 5-20 or 15-30
    paymentPeriod:{
        startDate: Date,
        endDate: Date
    },
    grossIncome:{
        // taken from User.Salary
        type:Number
    },
    // embedding para nababago ang amount for every user paySlip, ng hindi binabago ang amount sa basis na Incentive
    incentives:[{
        incentiveName:{
            type:String,
        },
        // let s = salary, r=rate
        incentiveFormula:{
            type:String,
        },
        incentiveAmount:{
            type: Number,
            default:0
        }
    }],
    deductions:[{
        deductionName:{
            type:String,
        },
        // let s = salary, r=rate
        deductionFormula:{
            type:String,
        },
        deductionAmount:{
            type: Number,
            default:0
        }
    }],
    // total amount of gross income and incentives, less deductions
    netPay:{
        type:Number
    }
}, {
    toJSON:{virtuals: true}
})

paySlipSchema.set('timestamps', true);


paySlipSchema.virtual('incentiveSubTotal').get(function () {
    let subtotal =0;
    for (let i = 0; i<this.incentives.length; i++){
        subtotal += this.incentives[i].incentiveAmount
    }

    return subtotal

})

paySlipSchema.virtual('deductionSubTotal').get(function () {
    let subtotal =0;
    for (let i = 0; i<this.deductions.length; i++){
        subtotal += this.deductions[i].deductionAmount
    }

    return subtotal

})
module.exports = mongoose.model('PaySlip', paySlipSchema);