require('dotenv').config();

const express = require('express');
const cors = require('cors')
const { reset } = require('nodemon');
const app = express();
const bodyParser = require("body-parser");
const path = require('path')
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const paySlipRoutes = require('./routes/paySlipRoutes');
const departmentRoutes = require('./routes/departmentRoutes');
const shiftRoutes = require('./routes/shiftRoutes');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static('public'));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended:true}));
app.use('/user', userRoutes);
app.use('/payslips',paySlipRoutes);
app.use('/departments',departmentRoutes);
app.use('/shifts', shiftRoutes);

// app.set('view engine', 'ejs');

const port = 5000;
mongoose.set(`strictQuery`,true)
mongoose.connect(process.env.connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology:true,
})
.then(()=>{console.log('connected to cloudDB');
app.listen(port, console.log(`Server started on port ${port}`))
})
.catch((error)=>{
    console.log(error);
})

