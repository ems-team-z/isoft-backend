const { Department, JobPosition, Deduction, Incentive } = require('../models/department');

const User = require('../models/user');
const auth = require('../auth');


// [START OF "DEPARTMENT" MODULES]
// WORKING
// CREATE DEPARTMENT MODULE
module.exports.createDepartment =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    let departmentName = req.body.name
    let newDepartment = new Department ({

      name: departmentName,
    })

    if(userData.isAdmin){
         let department = await Department.find({ 'name' : { '$regex' : departmentName , '$options' : 'i' }});

         if (department.length<1){
          return  newDepartment.save()
          .then(()=>res.status(201).json({message:`${departmentName} has been succesfully created.`, department:newDepartment}))
          .catch(error=>{
              console.log(error);
              res.status(400).json({message:`Sorry, there was an error during process. Please Try again!`})
          })
        }else{
            return res.status(200).json({message:`${departmentName} department already exists.`});
        }
    }
    return res.status(401).json({message: `You do not have admin privilege`})
}


// WORKING
// UPDATE DEPARTMENT MODULE
module.exports.updateDepartment =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    let departmentName = req.body.name
    let newName = req.body.newname
    let updatedDepartment= {
        name: newName,
        positions: req.body.newpositions,
        activeEmployeeCount: req.body.newactiveEmployeeCount
    }
    // { 'prodName' : { '$regex' : req.body.prodName , '$options' : 'i' }, isAvailable:true },{ prodName: 1, price: 1, description: 1, specs : 1, stocks: 1, isOnsale:1, discountedPrice:1} ).sort({prodName:1})
   
    if (userData.isAdmin){
        await Department.findOneAndUpdate({ 'name' : { '$regex' : departmentName , '$options' : 'i' }}, updatedDepartment, {new:true})
        .then(result=>{
            
            if(result){
      
                return res.status(200).json({message:"Updated Successfully", department: result})
            }

            return  res.status(404).json({message:"No record found."});
        })
        .catch(err=>{
           return res.status(400).send(err)
        })
    }
    else{
        return res.send("You do not have admin priveleges.")
    }
}


module.exports.addJobPositionToDeparment =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)
    let addedPositions = req.body.newpositions;
    let departmentName = req.body.name
    let departmentId;
    let updatedDepartmentPositions = {}
    // array for storing duplicate job entries already existing in the document.
    let jobsAlreadyIncluded = []

    if (userData.isAdmin){
        await Department.findOne({ 'name' : { '$regex' : departmentName , '$options' : 'i' }, isActive:true })
        .then(result=>{
            if(result){
                console.log(`This is result after finding.`,result);
                // assign the ID variable for later use in updating
                departmentId = result._id;

                // loops through input array of positions to check if input position already exists in the department
                addedPositions.forEach(position => {
                    if (result.positions.includes(position)){
                        console.log(`${position.name} position already exists in the department.`) 
                        jobsAlreadyIncluded.push(position)
                        
                    }
                    else{
                        // Adds the position if it doesn't exist in the current dept positions
                        result.positions.push(position)
                    }
                });

                // assign the updated positions to updatedDepartmentPosition variable for updating
                updatedDepartmentPositions.positions = result.positions
            }
            else{
                return  res.status(404).json({message:"No record found."});
            }
        })
        .then(()=>{
            Department.findByIdAndUpdate(departmentId, updatedDepartmentPositions, {new:true})
            .then((result)=>{
                // returns json of the updated Department with new job positions added
                return res.status(200).json({message:"Successfully added job positions.", department: result, "Positions that were attempted to add but already exists":jobsAlreadyIncluded})
            })
        })
        .catch(err=>{
           return res.status(400).send(err)
        })
    }
    else{
        return res.send("You do not have admin priveleges.")
    }
}


// WORKING
// GET ALL DEPARTMENTS MODULE
module.exports.getAllDepartments =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin){
        let departments = await Department.find({}).populate({
            path:     'positions',			
            populate: [{ path:  'incentives',
                    model: 'Incentive' },
                {
                    path:  'deductions',
                    model: 'Deduction' 
                }]
          })
        
        if(departments.length>0){
            return res.status(401).json({Message: `Retrieved ${departments.length} departments `, Departments:departments})
        }
        return res.status(404).json({Message:`No records found.`})

    }
    return res.status(401).json({Message: `You do not have admin privilege`})
}
// [END OF DEPARTMENT MODULES]



// [START OF "JOBPOSITION" MODULES]
// WORKING
// CREATE JOB POSITION MODULE
module.exports.createJobPosition =  async (req,res) =>{
    let jobTitle = req.body.name;
    let userData = auth.decode(req.headers.authorization)
    // taken from incentives array in frontend
    // can be done by having elements share the same name and take their values
    // https://www.geeksforgeeks.org/how-to-get-values-from-html-input-array-using-javascript/
    let incentives = req.body.incentives;
    let deductions = req.body.deductions;

    let newJobPosition = new JobPosition ({
        // departmentId:req.body.departmentId,
        name:jobTitle,
        salary: req.body.salary,
        incentives:[],
        deductions:[],
        // abolish job position 
    })
    console.log(`This is NEW:`,newJobPosition);


    if(userData.isAdmin){
    
         let jobPosition = await JobPosition.findOne({name:jobTitle});
      
        //  Check if incentives input is empty, and seeds newJobPosition.incentives if not
            if(incentives.length>0){
            incentives.forEach(incentive => {
                newJobPosition.incentives.push(incentive);
             });
            }
        
        //  Check if deductions input is empty, and seeds newJobPosition.deductions if not
            if(deductions.length>0){
             deductions.forEach(deduction=>{
                newJobPosition.deductions.push(deduction);
             })
            }
            
         if (!jobPosition){
          return  newJobPosition.save()
                  .then((savedPosition)=>{

                        return res.status(201).json({message:`${jobTitle} has been succesfully created.`, position:savedPosition})})
                 
                  .catch(error=>{
                   console.log(error);
                  res.status(400).json({message:`Sorry, there was an error during the creation of Job Position. Please Try again!`})
          })
        }else{
            return res.status(200).json({message:`${jobPosition.name} position already exists.`});
        }
    }
    return res.status(401).json({message: `You do not have admin privilege`})
}


// WORKING
// UPDATE POSITION MODULE
module.exports.updateJobPosition =  async (req,res) =>{
    /*  How to use
        1. Provide jobTitle represents the name of department to be updated
        2. Provide updatedJobTitle is the new department name if you want to change the current name
        3. Provide other incentives or deductions - adds incentives or deductions input

    */

    // NAME TO BE CHANGED - this can be changed later if team prefers to update by using name property
    // let jobTitle = req.body.name;

    
    let jobPositionId = req.params.jobPositionId
  
    let userData = auth.decode(req.headers.authorization)
    // NEW NAME
    let updatedJobTitle = req.body.newJobTitle;

    // taken from incentives array in frontend
    // can be done by having elements share the same name and take their values
    // https://www.geeksforgeeks.org/how-to-get-values-from-html-input-array-using-javascript/
    let inputIncentives = req.body.incentives;
    let inputDeductions = req.body.deductions;

    let updatedJobPosition = {
 
        name:updatedJobTitle,
        salary: req.body.salary,
        incentives:[],
        deductions:[],
        // abolish job position 
        isActive: req.body.isActive
        
    }

    if(userData.isAdmin){

            if(inputIncentives.length>0){
            inputIncentives.forEach(incentive => {
                updatedJobPosition.incentives.push(incentive);
             });
            }
            
            if(inputDeductions.length>0){
             inputDeductions.forEach(deduction=>{
               updatedJobPosition.deductions.push(deduction);
             })            
            }
        

        try {
            await JobPosition.findByIdAndUpdate(jobPositionId, updatedJobPosition, {new:true}).then(result=>{
                if(result){
                    return res.status(200).json({message:"Updated Successfully", jobPosition: result})
                }
    
                return  res.status(404).json({message:"No record found."});
            })
            .catch(error=>{
               return res.status(400).send(error)
            })
        } catch (error) {
           console.log(error)
        }
        
    }
    else{
        return res.send("You do not have admin priveleges.")
    }
}



// WORKING
// GET ALL JOB POSITION MODULE
module.exports.getAllJobPositions =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin){
            // sorts shows Active positions first then sort them by name in ascending order
        let jobPositions = await JobPosition.find({}).sort({isActive:-1, name:1}).populate([
            {
              path: 'incentives',
              model: 'Incentive',

            },
            {
                path: 'deductions',
                model: 'Deduction',
  
              }])
        
        if(jobPositions.length>0){
            return res.status(401).json({Message: `Retrieved ${jobPositions.length} results `, "Job Positions":jobPositions})
        }
        return res.status(404).json({Message:`No records found.`})

    }
    return res.status(401).json({Message: `You do not have admin privilege`})
}
// [END OF "JOBPOSITION" MODULES]



// [START OF "INCENTIVE" MODULES]
// https://stackoverflow.com/questions/14263024/convert-equation-string-to-equation
// WORKING
// GET ALL INCENTIVES
module.exports.getAllIncentives =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin){
            // sorts shows Active positions first then sort them by name in ascending order
        let incentives = await Incentive.find({})
        
        if(incentives.length>0){
            return res.status(401).json({Message: `Retrieved ${incentives.length} results `, "Job Positions":incentives})
        }
        return res.status(404).json({Message:`No records found.`})

    }
    return res.status(401).json({Message: `You do not have admin privilege`})
}

//WORKING
// CREATE NEW INCENTIVE
module.exports.createIncentive = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)
    let name = req.body.incentiveName;
    let formula = req.body.incentiveFormula;
    let newIncentive = new Incentive({
        incentiveName: name,
        incentiveFormula: formula,
    })

    if(userData.isAdmin){

        let incentive = await Incentive.findOne({incentiveName:name})
        console.log(incentive);
        if(!incentive){
            newIncentive.save()
            .then(result=> {
                return res.status(201).json({message:`${result.incentiveName} incentive has been created.`, incentive:result})
            })
            .catch(error => {return res.send(error)});
        }
        else{
            return res.status(200).json({message:`${name} already exists.`})
        }
        
    }
    else{
        return res.status(401).json({message:`You do not have admin privilege.`})
    }
}


//WORKING
// UPDATE INCENTIVE
module.exports.updateIncentive = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)
    let name = req.body.name;
    let formula = req.body.incentiveFormula;
    let newIncentiveName = req.body.newincentiveName
    let updatedIncentive = {
        incentiveName: newIncentiveName,
        incentiveFormula: formula,
        incentiveAmount: req.body.incentiveAmount,
        isActive: req.body.isActive
    }

    if(userData.isAdmin){

        try {
            await Incentive.findOneAndUpdate({incentiveName:name}, updatedIncentive, {new:true}) 
            .then(result=> {
                return res.status(200).json({message:`${name} incentive has been updated.`, incentive:result})
            })
        } 
            catch (error) {
            return res.send(error)
        }
    }
    else{
        return res.status(401).json({message:`You do not have admin privilege.`})
    }
}
// [END OF INCENTIVE MODULES]



// [START OF DEDUCTION MODULES]
// GET ALL DEDUCTIONS
module.exports.getAllDeductions =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin){
  
        let deduction = await Deduction.find({})
        
        if(deduction.length>0){
            return res.status(401).json({Message: `Retrieved ${deduction.length} results `, "Job Positions":deduction})
        }
        return res.status(404).json({Message:`No records found.`})

    }
    return res.status(401).json({Message: `You do not have admin privilege`})
}


// CREATE NEW DEDUCTION
module.exports.createDeduction = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)
    let name = req.body.deductionName;
    let formula = req.body.deductionFormula;
    let newDeduction = new Deduction({
        deductionName: name,
        deductionFormula:formula,
        deductionAmount:0
    })

    if(userData.isAdmin){

        let deduction = await Deduction.findOne({deductionName:name})

        if(!deduction){
            newDeduction.save()
            .then(result=> res.status(201).json({message:`${result.deductionName} incentive has been created.`, deduction:result}))
            .catch(error => res.send(error));
        }
        else{
             return res.status(200).json({message:`${name} already exists.`})
          }
    }
    else{
        return res.status(401).json({message:`You do not have admin privilege.`})
    }
}


//WORKING
// UPDATE DEDUCTION
module.exports.updateDeduction = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)
    let name = req.body.name;
    let formula = req.body.deductionFormula;
    let newDeductionName = req.body.newdeductionName
    let updatedDeduction= {
        deductionName: newDeductionName,
        deductionFormula: formula,
        deductionAmount: req.body.deductionAmount,
        isActive: req.body.isActive
    }

    if(userData.isAdmin){

        try {
            await Deduction.findOneAndUpdate({deductionName:name}, updatedDeduction, {new:true}) 
            .then(result=> {
                return res.status(200).json({message:`${name} deduction has been updated.`, deduction:result})
            })
        } 
            catch (error) {
            return res.send(error)
        }
    }
    else{
        return res.status(401).json({message:`You do not have admin privilege.`})
    }
}


// [END OF DEDUCTION MODULES]