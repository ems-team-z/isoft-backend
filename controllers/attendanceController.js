const Attendance = require("../models/attendance")
const User = require("../models/user")
const auth = require("../auth")

const userAttendance = async (req, res) => {


    const token = req.headers.authorization
    const userData = auth.decode(token)

    const employeeData = await User.findById(userData._id)
    const employeeFullName = `${employeeData.firstName} ${employeeData.middleName} ${employeeData.lastName}`
    const dateToday = new Date().toLocaleDateString();
    const currentTime = new Date().toLocaleTimeString();

    const userAttendanceRecord = await Attendance.findOne({date: dateToday, userId: userData._id})

    const todayTimeInOut = await new Attendance({
        department: employeeData.companyProfile.departmentId, //ref to User.department
        userId: employeeData._id, 
        shift: "pm/am", //ref to User.shift
        timeIn: currentTime,
        date: dateToday
    })

    if(!userAttendanceRecord){
        const newTimeIn = await Attendance.create(todayTimeInOut);
        return res.status(200).json(newTimeIn);
    }

    if(!userAttendanceRecord.timeOut){
        userAttendanceRecord.timeOut = currentTime;

        let start = userAttendanceRecord.timeIn;
        let end = currentTime;
        start = start.split(":");
        end = end.split(":");
        let startDate = new Date(0, 0, 0, start[0], start[1], 0);
        let endDate = new Date(0, 0, 0, end[0], end[1], 0);
        let diff = endDate.getTime() - startDate.getTime();
        let hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        let minutes = Math.floor(diff / 1000 / 60);
        minutes -= userAttendanceRecord.breakConsumed;

        userAttendanceRecord.status = (minutes > 450) ? "Goods" : "UnderTime"

        userAttendanceRecord.save();
        return res.status(200).send(userAttendanceRecord)
    }
    res.send("You have finish your needed work time for today. Comeback on your next workday!")
}
let start = null;
let end = null;
const breakTime = async (req, res) => {
    const token = req.headers.authorization
    const userData = auth.decode(token)
    const dateToday = new Date().toLocaleDateString();
    const userAttendanceRecord = await Attendance.findOne({date: dateToday, userId: userData._id})

    if(userAttendanceRecord){
        if(start === null){
            start = new Date().toLocaleTimeString()
            res.send("Break time start")
        }
        else{
            end = new Date().toLocaleTimeString()
            start = start.split(":");
            end = end.split(":");
            let startDate = new Date(0, 0, 0, start[0], start[1], 0);
            let endDate = new Date(0, 0, 0, end[0], end[1], 0);
            let diff = endDate.getTime() - startDate.getTime();
            let hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            let minutes = Math.floor(diff / 1000 / 60);
            userAttendanceRecord.breakConsumed += minutes
            await userAttendanceRecord.save()
            start = null
            end = null
            res.send("Break time end")
        } 
    }
    else{
        res.status(400).json("You haven't started yet your shift today!")
    }

}


const dailyTimeRecord = async (req, res) => {
    const employeeDTR = await Attendance.find({employeeId: "userData._id"})
    res.status(200).json(employeeDTR)
}


module.exports = {
    userAttendance,
    dailyTimeRecord,
    breakTime
}

