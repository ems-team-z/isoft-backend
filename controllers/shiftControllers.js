
const Shift = require('../models/shift');
const auth = require('../auth');


// {
//     nameShift:{
//         type:String,
//         require: true
//     },
//     // expected pasok
//     expectedStartTime:{
//         type: Date,
      
//     },
//     // expected awasan
//     expectedEndTime:{
//         type:Date,
       
//     },
//     // disable enable SHIFT
//     isActive:{
//         type:Boolean,
//         default:true
//     }
// }
module.exports.getAllShifts =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)


    if(userData.isAdmin){


         await Shift.find({})
          .then((result)=>{
            if(result.length>0){
                return res.status(201).json({message:`Retrieved ${result.length} shift record(s).`, shifts:result})
            }
            else{
                return res.status(404).json({message:`No records found.`})
            }
           })
          .catch(error=>{
              console.log(error);
              return res.status(400).json({message:`Sorry, there was an error during process. Please Try again!`})
          })
    }
    else{
        return res.status(401).json({message: `You do not have admin privilege`})
    }
    
}




module.exports.createShift =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    let shiftName = req.body.nameShift

    
    let newShift = new Shift ({
    
      nameShift: shiftName,
      expectedStartTime : req.body.expectedStartTime,
      expectedEndTime : req.body.expectedEndTime,
      isActive: req.body.isActive
    })

    if(userData.isAdmin){
         let shift = await Shift.findOne({nameShift:shiftName});

         if (!shift){
          return  newShift.save()
          .then(()=>res.status(201).json({message:`${shiftName} has been succesfully created.`, shift:newShift}))
          .catch(error=>{
              console.log(error);
              res.status(400).json({message:`Sorry, there was an error during process. Please Try again!`})
          })
        }else{
            return res.status(200).json({message:`${shiftName} shift already exists.`});
        }
    }
    return res.status(401).json({message: `You do not have admin privilege`})
}


module.exports.updateShift =  async (req,res) =>{
    let userData = auth.decode(req.headers.authorization)

    let shiftName = req.body.nameShift
    let shiftId = req.body.shiftId;
    let updatedShift = ({
      
      nameShift: req.body.newShiftName,
      expectedStartTime : req.body.expectedStartTime,
      expectedEndTime : req.body.expectedEndTime,
   
    })

    if(userData.isAdmin){
        //  let shift = await Shift.findOne({nameShift:shiftName});

        //  if (shift){
        //     //sets the shiftId to be used in updating
        //     shiftId = shift._id


            Shift.findByIdAndUpdate(shiftId, updatedShift, {new:true}) 
          .then(()=>res.status(200).json({message:`${shiftName} has been succesfully updated.`, shift:updatedShift}))
          .catch(error=>{
              console.log(error);
                return res.status(400).json({message:`Sorry, there was an error during process. Please Try again!`})
          })
        // }else{
        //     return res.status(200).json({message:`${shiftName} shift doesn't exist.`});
        // }
    }
    else{return res.status(401).json({message: `You do not have admin privilege`})}
}