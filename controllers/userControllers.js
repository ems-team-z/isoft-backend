const User = require('../models/user');
const Payslip = require('../models/paySlip')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Notification = require('../models/notification')
const ObjectId = require('mongoose').Types.ObjectId;
const Attendance = require('../models/attendance')




        // check if email format is valid
module.exports.isEmail =  (req, res, next)=>{

                let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                let emailAddress = req.body.contacts.email;

                if (emailAddress.match(regex)) {
                     next();
                }
                else { 
                    return res.send(`Enter a valid email address.`)
                }
            }
        


module.exports.checkEmailExists =  async (req, res, next)=>{

    let emailAdd = req.body.contacts.email;
        console.log(emailAdd)
        await User.find( {"contacts.email":emailAdd} ).then(result=>{
            console.log(result);
            let message = "";
            if(result.length > 0){
                message = `The ${emailAdd} is already taken, please use a different email.`
                return res.status(200).send(message)
            }  
            else{ next(); }
        }).catch(error => res.send(error))
} 

// LOGIN
// ROUTE: /login
module.exports.loginUser = async (req,res) =>{
    const loginCreds = await User.findOne({"contacts.email": req.body.email});
        
    if(!loginCreds){
        return res.status(404).json(`The email you entered is not yet registered. Please register first!`);
    }
    else{
        const isPasswordCorrect = await bcrypt.compareSync(req.body.password, loginCreds.password);

        if(isPasswordCorrect){
            res.status(200).json({accessToken: auth.createAccessToken(loginCreds)});
        }
        else{
            res.status(404).json(`Password is incorrect!`);
        }
    }

}

// REGISTER
// ROUTE: /register
module.exports.registerUser =  async (req,res) =>{
 
      let newUser = new User ({

        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        birthDate:req.body.birthDate,
        civilStatus:req.body.civilStatus,
        contacts:{
            mobileNum: req.body.contacts.mobileNum ,
            email:req.body.contacts.email
        },
        password: bcrypt.hashSync(req.body.password, 10),
        companyProfile:{
            departmentId: req.body.companyProfile.departmentId,
            jobPositionId: req.body.companyProfile.jobPositionId
        },
        address:{
            province: req.body.address.province,
            municipality: req.body.address.municipality,
            barangay:  req.body.barangay,
            houseNum:  req.body.houseNum,
            street:  req.body.street,
            zipcode: req.body.zipcode
        }
    })

            return  newUser.save()
            .then(()=>res.status(201).json({message:`You have succesfully registered.`, details:newUser}))
            .catch(error=>{
                console.log(error);
                res.status(400).json({message:`Sorry, there was an error during the registration. Please Try again!`})
            })
}


module.exports.updateUser =  async (req,res) =>{
    /*This module can:  
        EDIT any properties of a USER
        
    */

    // used for updating by Id
    let userId = req.body.userId;

    let updatedUser = {

      firstName: req.body.firstName,
      middleName: req.body.middleName,
      lastName: req.body.lastName,
      birthDate:req.body.birthDate,
      civilStatus:req.body.civilStatus,
      contacts:{
          mobileNum: req.body.contacts.mobileNum ,
          email:req.body.contacts.email
      },
      password: bcrypt.hashSync(req.body.password, 10),
      companyProfile:{
          departmentId: req.body.companyProfile.departmentId,
          jobPositionId: req.body.companyProfile.jobPositionId
      },
      address:{
          province: req.body.address.province,
          municipality: req.body.address.municipality,
          barangay:  req.body.barangay,
          houseNum:  req.body.houseNum,
          street:  req.body.street,
          zipcode: req.body.zipcode
      }
  }     
        let user = await User.findById(userId)

        if(user){

            await User.findByIdAndUpdate(userId, updatedUser, {new:true}).populate([{
                path:     'companyProfile.departmentId',
                model:  'Department',
                select: 'name _id'},
                { 
                path:'companyProfile.jobPositionId',
                model:'JobPosition',
                select: '_id name salary',
                populate: [{ path:  'incentives',
                        model: 'Incentive' ,
                        select: '_id incentiveName'},
                    {
                        path:  'deductions',
                        model: 'Deduction',
                        select: '_id deductionName'
                    }]
              }])
            .then((result)=>{
                
                return res.status(200).json({message:`You have succesfully updated user ${updatedUser.firstName} ${updatedUser.lastName}.`, details:result})
            })
            .catch(error=>{
                console.log(error);
                res.status(400).json({message:`Sorry, there was an error during the process. Please Try again!`})
            })

        }
        else{
            return res.status(404).json({message:"User does not exist"})
        }          
}




module.exports.getAllUser = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin)
    {
                   //DEBUG THISS!! CANT RETURN DESIRED FIELDS IN POPULATE      //this is populate syntax for find
        await User.find({}).populate([{
            // populates the user's department Info: displays the name and id of the department
            path:     'companyProfile.departmentId',
            model: 'Department',
            select: 'name _id'},
            { 
            // populates the user's jobinformation, selected fields to display can be changed in the select property, separate the fields with space
            path:'companyProfile.jobPositionId',
            model:'JobPosition',
            select: '_id name salary',
            // [uncomment this to populate incentives and deductions as well]
            populate: [{ path:  'incentives',
                    model: 'Incentive' ,
                    select: '_id incentiveName'},
                {
                    path:  'deductions',
                    model: 'Deduction',
                    select: '_id deductionName'
                }]
          }])
        .then(result=>
            {return res.status(200).json({Retrieved:result.length, users:result})}
        )
        .catch(error=>{
            res.status(204).json({message:`No users found.`, error:error})
        })
    }
    else{
        return res.send("You do not have admin priveleges.");
    }
}

// User only it will send notifications to admin
module.exports.forgotPassword = async (req, res) => {
    const user = await User.findOne({"contacts.email": req.body.email})
    if(!user)
        "Email is not register!"
    else{
        if(user.isAdmin){
            res.send("Redirect to change password form(admin only)")
        }
        else{
            const admins = await User.find({isAdmin: true})
            
            for(let i = 0; i < admins.length; i++){
                let newNotification = await new Notification({
                    userId: admins[i].userId,
                    name: `Employee with the ${req.body.email} email is requesting for a reset password!`,
                })
                Notification.create(newNotification)
            }
            res.status(200).json("Request for change password has sent to admin!")
        }
    }
}

// Admin only. when admin forgot password
module.exports.changePassword = async (req, res) => {
    const user = await User.findOne({"contacts.email": req.params.email})
    if(req.body.newPassword === req.body.confirmPassword){
        user.password = await bcrypt.hashSync(req.body.newPassword, 10)
        await user.save()
        return res.status(200).json("Password Successfully Changed!")
    }
    else{
        return res.status(400).json("Password doesn't match!")
    }
}

// Admin only Reset the user password to default: iSoftESS2022
module.exports.resetPassword = async(req, res) => {
    
    const employee = await User.findByIdAndUpdate(req.params.employeeId, {password: bcrypt.hashSync("iSoftESS2022", 10)}, {new: true})
    
    if(employee){
       return res.status(200).json(employee)
    }
    return res.status(404).json(`No user found!`)
}




// UPDATE User ROLE details
// ROUTE: /updaterole
module.exports.updateRole = async (req,res)=>{

    const employeeId = req.params.employeeId;
        if(!ObjectId.isValid(employeeId)){
            return res.status(400).json(`Product Id ${employeeId} is not valid objectId!`);
        }
        const token = req.headers.authorization
        const userData = auth.decode(token)

        if(userData.isAdmin){
            const result = await User.findById(employeeId)
            const updatedUser = await User.findByIdAndUpdate(employeeId, {isAdmin: !result.isAdmin}, {new: true})
            if(updatedUser){
            res.status(200).json(updatedUser)
            }
            else{
                res.status(400).json("User not found!")
            }
        }
        else{
            res.status(400).json("You dont have acces on this page")
        }


}


// VIEW User Profile details
// ROUTE: /viewprofile
module.exports.profileDetails = async (req,res) =>{
    // user will be object that contains the id and email of the user that is currently logged in.
    const userData = auth.decode(req.headers.authorization);

    await User.findById(userData._id).populate('companyProfile.jobPositionId')
    .then(user=>{
        // if no user found
        if(!user){
            return res.status(200).json({message:`No user found.`});
        }

        user.password = "Confidential";
        return res.status(200).json({Profile:user})
    }).catch(err => {
        return res.send(err);
    })
}


module.exports.userAttendance = async (req, res) => {


    const token = req.headers.authorization
    const userData = auth.decode(token)

    const employeeData = await User.findById(userData._id)
    const employeeFullName = `${employeeData.firstName} ${employeeData.middleName} ${employeeData.lastName}`
    const dateToday = new Date().toLocaleDateString();
    const currentTime = new Date().toLocaleTimeString();

    const userAttendanceRecord = await Attendance.findOne({date: dateToday, userId: userData._id})

    const todayTimeInOut = await new Attendance({
        department: employeeData.companyProfile.departmentId, //ref to User.department
        userId: employeeData._id, 
        shift: "pm/am", //ref to User.shift
        timeIn: currentTime,
        date: dateToday
    })

    if(!userAttendanceRecord){
        const newTimeIn = await Attendance.create(todayTimeInOut);
        return res.status(200).json(newTimeIn);
    }

    if(!userAttendanceRecord.timeOut){
        userAttendanceRecord.timeOut = currentTime;

        let start = userAttendanceRecord.timeIn;
        let end = currentTime;
        start = start.split(":");
        end = end.split(":");
        let startDate = new Date(0, 0, 0, start[0], start[1], 0);
        let endDate = new Date(0, 0, 0, end[0], end[1], 0);
        let diff = endDate.getTime() - startDate.getTime();
        let hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        let minutes = Math.floor(diff / 1000 / 60);
        minutes -= userAttendanceRecord.breakConsumed;

        userAttendanceRecord.status = (minutes > 450) ? "Goods" : "UnderTime"

        userAttendanceRecord.save();
        return res.status(200).send(userAttendanceRecord)
    }
    res.send("You have finish your needed work time for today. Comeback on your next workday!")
}

// For breakTime variable initial start time/end
let start = null;
let end = null;
module.exports.breakTime = async (req, res) => {
    const token = req.headers.authorization
    const userData = auth.decode(token)
    const dateToday = new Date().toLocaleDateString();
    const userAttendanceRecord = await Attendance.findOne({date: dateToday, userId: userData._id})

    // if(userAttendanceRecord.timeOut){ 
    //     return res.status(400).json("you are dont with your shift for today!")
    // }
    if(userAttendanceRecord){
        if(start === null){
            start = new Date().toLocaleTimeString()
            console.log(start)
            res.send("Break time start")
        }
        else{
            end = new Date().toLocaleTimeString()
            console.log(end)
            start = await start.split(":");
            end = await end.split(":");
            let startDate = await new Date(0, 0, 0, start[0], start[1], 0);
            let endDate = await new Date(0, 0, 0, end[0], end[1], 0);
            let diff = await endDate.getTime() - startDate.getTime();
            let hours = await Math.floor(diff / 1000 / 60 / 60);
            diff -= await hours * 1000 * 60 * 60;
            let minutes = await Math.floor(diff / 1000 / 60);
            console.log(minutes)
            userAttendanceRecord.breakConsumed += await minutes
            await userAttendanceRecord.save()
            start = null
            end = null
            res.send("Break time end")
        } 
    }
    else{
        res.status(400).json("You haven't started yet your shift today!")
    }

}


module.exports.dailyTimeRecord = async (req, res) => {
    const employeeDTR = await Attendance.find({employeeId: "userData._id"})
    res.status(200).json(employeeDTR)
}




// VIEW OWN PAYSLIP
// ROUTE /viewpayslip
module.exports.viewPaySlip = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let userId = userData._id;

    // // if no provided
    // if (!userData){
    //     userData.id = req.body.userId;
    // }

    await User.findById(userId).populate('paySlips' ).sort('createdAt')
    .then(result=>{
        
        if(result.paySlips.length < 1){
            return res.status(200).json({message:"No record found"});
        }

        return res.status(200).json({
            count : result.paySlips.length,
            Payslips:result.paySlips});


    }).catch(err => {
        return res.send(err);
    })
}


// NEEDS TO BE DEBUGGED AND TESTED
// VIEW OTHER USER'S PAYSLIP 
//ROUTE: /
module.exports.viewOtherPaySlip = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let userId = userData._id;

    // // if no provided
    // if (!userData){
    //     userData.id = req.body.userId;
    // }

    await User.findById(userId).populate('paySlips' ).sort('createdAt')
    .then(result=>{
        
        if(result.paySlips.length < 1){
            return res.status(200).json({message:"No record found"});
        }

        return res.status(200).json({
            count : result.paySlips.length,
            Payslips:result.paySlips});


    }).catch(err => {
        return res.send(err);
    })
}


