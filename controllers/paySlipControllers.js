const PaySlip = require('../models/paySlip');
const User = require('../models/user');
const auth = require('../auth');
const { Department, JobPosition, Deduction, Incentive } = require('../models/department');
const Parser = require('expr-eval').Parser;
const parser = new Parser();


function roundToTenths(num){
    return Math.round(num*100)/100
}


module.exports.generatePaySlip =  async (req,res) =>{
    const t1 = performance.now();//for testing performance only, will be removed later
    let userId = req.body.userId;

    // for PERIOD COVERED
    let startdate = new Date (req.body.paymentPeriod.startDate);
    let enddate = new Date (req.body.paymentPeriod.endDate);

    // These two variable will be used in computing the netPay
    let incentiveSubTotal =0;
    let deductionSubTotal=0;
    
    let userSalary;
    let userDailyRate;
    let userHourlyRate;
    let userOverTimeRendered = 0;
    let userIncentives;
    let userDeductions;
    // evaluates the String formula in each Incentive and Deductions
  
    let newPaySlip = new PaySlip ({
        userId: userId,
        paymentPeriod:{
            startDate: startdate,
            endDate:enddate,
        },
        grossIncome: 0,
        incentives: [],
        deductions: []
  })
        // Get user's data
        let user = await User.findById(userId).populate([{
            // populates the user's department Info: displays the name and id of the department
            path:     'companyProfile.departmentId',
            model: 'Department',
            select: 'name _id'},
            { 
            // populates the user's jobinformation, selected fields to display can be changed in the select property, separate the fields with space
            path:'companyProfile.jobPositionId',
            model:'JobPosition',
            select: '_id name salary',
            // [uncomment this to populate incentives and deductions as well]
            populate: [{ path:  'incentives',
                    model: 'Incentive' ,
                    select: '-_id +incentiveName +incentiveFormula +incentiveAmount'},
                {
                    path:  'deductions',
                    model: 'Deduction',
                    select: '-_id +deductionName +deductionFormula +deductionAmount'
                }]
          },
          {
            // populates the user's department Info: displays the name and id of the department
            path:     'paySlips',
            model: 'PaySlip',
            select: 'paymentPeriod'
          }
        ])

          if(!user){
            return res.status(404).json({message:"No user found."})
          }
             //  variable to shorten access to user's jobPosition properties
          let userJobDetails = user.companyProfile.jobPositionId

          userSalary = userJobDetails.salary;
          userDailyRate = roundToTenths(userSalary/26);
          userHourlyRate = roundToTenths(userDailyRate/8) ;
          userIncentives = userJobDetails.incentives;
          userDeductions = userJobDetails.deductions;
  
  
            //assign half of user salary to gross Income which is equivalent to 1 cut-off
          newPaySlip.grossIncome = roundToTenths(userSalary/2);  
          const evaluateFormula = function (formula){ return roundToTenths(parser.parse(formula).evaluate({salary:userSalary, dailyRate:userDailyRate, hourlyRate:userHourlyRate, overTimeRendered:userOverTimeRendered}))}

        //   INCENTIVE SUBTOTAL required for NETPAY computation
          userIncentives.forEach(incentive => {
 
                if(incentive.incentiveAmount>0){
                    incentiveSubTotal += incentive.incentiveAmount
                }
                else{
                    // computes the incentive amount based on the formula property
                    incentive.incentiveAmount = evaluateFormula(incentive.incentiveFormula);
                    incentiveSubTotal += incentive.incentiveAmount
                }
                console.log(`THIS IS AN INCENTIVE`,incentive)
                // newPaySlip.incentives.push(incentive)//kasama dito id
                //eto wala id panget nga lang
                newPaySlip.incentives.push({ 
                    incentiveName: incentive.incentiveName,
                    incentiveFormula: incentive.incentiveFormula,
                    incetiveAmount : incentive.incentiveAmount
                })
       
          });
         
        //  DEDUCTION SUBTOTAL required for NETPAY computation
          userDeductions.forEach(deduction =>{
            if(deduction.deductionAmount>0){
                deductionSubTotal += deduction.deductionAmount
            }
            else{
                // computes the deduction amount based on the formula property
                deduction.deductionAmount = evaluateFormula(deduction.deductionFormula);
                deductionSubTotal += deduction.deductionAmount
            }
            console.log(`THIS IS AN DEDUCTIONS`,deduction)
            // newPaySlip.deductions.push(deduction);
            newPaySlip.deductions.push({ 
                deductionName: deduction.deductionName,
                deductionFormula: deduction.deductionFormula,
                deductionAmount : deduction.deductionAmount
            })
      
          })

          //   assign new key:value pair to newPaySlip incentives and deduction 
        //   newPaySlip.incentives = userIncentives;
        //   newPaySlip.deductions = userDeductions;
        //   For testing code only can be removed LATER.
        //   console.log(`NEWPAYSLIP INCENTINVES`,newPaySlip.incentives);
        //   console.log(`NEWPAYSLIP INCENTINVES`,userDeductions);

          newPaySlip.netPay = incentiveSubTotal + newPaySlip.grossIncome - deductionSubTotal;

        //   REMOVE THIS LATER
      const t2 = performance.now();
      console.log(t2-t1, 'milliseconds')
      
          console.log(`This is the newPayslip before saving`,newPaySlip);
        //   SAVING PAYSLIP
          return  newPaySlip.save()
          .then((payslip)=>{
            // UPDATING USER PAYSLIP HISTORY
            user.paySlips.push(payslip._id);
            console.log(`User after push`,user);
            user.save();
            res.status(201).json({message:`Successfully generated Payslip for ${user.firstName} ${user.lastName}.`, details:newPaySlip})
          })
          .catch(error=>{
              console.log(error);
              res.status(400).json({message:`Sorry, there was an error during the registration. Please Try again!`})
          })

}


module.exports.generatePaySlipAllActiveEmployee =  async (req,res) =>{
    const t1 = performance.now();


    // for PERIOD COVERED
    let startdate = new Date (req.body.paymentPeriod.startDate);
    let enddate = new Date (req.body.paymentPeriod.endDate);


        // Get user's data
        let users = await User.find({isActive:true}).populate([{
            // populates the user's department Info: displays the name and id of the department
            path:     'companyProfile.departmentId',
            model: 'Department',
            select: 'name _id'},
            { 
            // populates the user's jobinformation, selected fields to display can be changed in the select property, separate the fields with space
            path:'companyProfile.jobPositionId',
            model:'JobPosition',
            select: '_id name salary',
            populate: [{ path:  'incentives',
                    model: 'Incentive' ,
                    select: '-_id +incentiveName +incentiveFormula +incentiveAmount'},
                {
                    path:  'deductions',
                    model: 'Deduction',
                    select: '-_id +deductionName +deductionFormula +deductionAmount'
                }]
          },
          {
            // populates the user's department Info: displays the name and id of the department
            path:     'paySlips',
            model: 'PaySlip',
            select: 'paymentPeriod'
          }
        ])
        console.log(users)
          if(users.length<1){
            return res.status(404).json({message:"No user(s) found."})
          }
             
        //[START]  
        await users.forEach(user => {
        
            // These two variable will be used in computing the netPay
        let incentiveSubTotal =0;
        let deductionSubTotal=0;
        let userId = user._id;
        let userJobDetails = user.companyProfile.jobPositionId
        
        let userSalary = userJobDetails.salary;
        let userDailyRate = roundToTenths(userSalary/26);
        let userHourlyRate = roundToTenths(userDailyRate/8) ;
        let userOverTimeRendered = 0;
        let userIncentives = userJobDetails.incentives;
        let userDeductions = userJobDetails.deductions;
        // evaluates the String formula in each Incentive and Deductions
        
        let newPaySlip = new PaySlip ({

            userId: userId,
            paymentPeriod:{
                startDate: startdate,
                endDate:enddate,
            },
            grossIncome: 0,
            incentives: [],
            deductions: []
        })    

            //assign half of user salary to gross Income which is equivalent to 1 cut-off
          newPaySlip.grossIncome = roundToTenths(userSalary/2);  

          // converts the string formula into an arithmetic expression  
          const evaluateFormula = function (formula){ return roundToTenths(parser.parse(formula).evaluate({salary:userSalary, dailyRate:userDailyRate, hourlyRate:userHourlyRate, overTimeRendered:userOverTimeRendered}))}

         // only executes if there are deductions
          if(userIncentives.length>0){
          userIncentives.forEach(incentive => {
 
                if(incentive.incentiveAmount>0){
                    incentiveSubTotal += incentive.incentiveAmount
                }
                else{
                    // computes the incentive amount based on the formula property
                    incentive.incentiveAmount = evaluateFormula(incentive.incentiveFormula);
                    incentiveSubTotal += incentive.incentiveAmount
                }
                console.log(`THIS IS AN INCENTIVE`,incentive)
                newPaySlip.incentives.push(incentive)//kasama dito id dapat wala eh dedebug pa
       
          });
        }
        
        // only executes if there are deductions
        if(userDeductions.length>0){
          userDeductions.forEach(deduction =>{
            
            if(deduction.deductionAmount>0){
                deductionSubTotal += deduction.deductionAmount
            }
            else{
                // computes the deduction amount based on the formula property
                deduction.deductionAmount = evaluateFormula(deduction.deductionFormula);
                deductionSubTotal += deduction.deductionAmount
            }
            newPaySlip.deductions.push(deduction);
          })
        }
        //   console.log(`NEWPAYSLIP INCENTINVES`,newPaySlip.incentives);
        //   console.log(`NEWPAYSLIP INCENTINVES`,userDeductions);
          newPaySlip.netPay = incentiveSubTotal + newPaySlip.grossIncome - deductionSubTotal;   //   Generating Net Pay


            const t2 = performance.now();
            console.log(t2-t1, 'milliseconds')
                // SAVING PAYSLIP 
                newPaySlip.save()
                .then(async(payslip)=>{
                    // UPDATING USER'S PAYSLIP HISTORY
                    user.paySlips.push(payslip._id);
                    console.log(`User after push`,user);
                    await user.save();
                   
                })
                .catch(error=>{
                    console.log(error);
                    res.status(400).json({message:`Sorry, there was an error during the registration. Please Try again!`})
                })

        });         
        
        res.status(201).json({message:`Successfully generated Payslip for all ${users.length} Active Employees.`})
}





// GET PAYSLIP BY ID
module.exports.getPaySlipById = async (req,res)=>{
  let userData = auth.decode(req.headers.authorization)
  
    let paySlipId = req.body.paySlipId

    if(userData.isAdmin)
    {

        await PaySlip.find({paySlipId}).populate('userId', ['firstName' ,'lastName','companyProfile'])
        .then(result=>
            {return res.status(200).json({retrieved:result.length, payslips:result})}
            )
        .catch(error=>{
            res.status(204).json({message:`No users found.`, error:error})
        })
    // }
 
    }
       else{
        return res.send("You do not have admin priveleges.");
    }
}


// GET ALL PAYSLIPS
// ROUTE /
module.exports.getAllPaySlips = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)

      if(userData.isAdmin)
      {
  
          await PaySlip.find({}).select('').populate([
            {
            // populates the user's department Info: displays the name and id of the department
            path:     'userId',
            model: 'User',
            select: 'lastName firstName',
            populate:[{
                        path:'companyProfile.departmentId',
                        model:'Department',
                        select: 'name'
                    }, 
                    { 
                        // populates the user's jobinformation, selected fields to display can be changed in the select property, separate the fields with space
                        path:'companyProfile.jobPositionId',
                        model:'JobPosition',
                        select: '_id name salary',
                    }
            ]}
        ])
          .then(result=>
              {return res.status(200).json({retrieved:result.length, payslips:result})}
              )
          .catch(error=>{
              res.status(204).json({message:`No users found.`, error:error})
          })
      // }
   
      }
         else{
          return res.send("You do not have admin priveleges.");
      }
  }