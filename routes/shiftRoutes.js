const express = require('express');
const router = express.Router();
const auth = require('../auth');
const shiftController = require('../controllers/shiftControllers');


// ss
// DEPARTMENT ENDPOINTS HERE
// ENDPOINT GET ALL SHIFT
router.get('/', auth.verify, shiftController.getAllShifts);


// http://localhost:5000/shifts/create
//ENDPOINT CREATE SHIFT
router.post('/create', auth.verify, shiftController.createShift);


// http://localhost:5000/shifts/update
// ENDPOINT UPDATE  IN SHIFT
router.put('/update', auth.verify, shiftController.updateShift);



module.exports = router;