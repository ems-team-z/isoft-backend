const express = require("express");

const router = express.Router();

const {
    userAttendance,
    dailyTimeRecord,
    breakTime
} = require("../controllers/attendanceController")

router.post("/timeInOut", userAttendance)
router.get("/dailyTimeRecord", dailyTimeRecord)
router.patch("/breakInOut", breakTime)

module.exports = router;
