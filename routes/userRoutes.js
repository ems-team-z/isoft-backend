const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userControllers');



router.get('/', auth.verify, userController.getAllUser);

router.post('/login', userController.loginUser)

router.post('/register',userController.isEmail, userController.checkEmailExists, userController.registerUser);

// UPDATE A USER
router.put('/update', auth.verify, userController.updateUser );

// VIEW USER'S OWN PAYSLIP
router.get('/viewpayslip', auth.verify, userController.viewPaySlip );

// VIEW USER'S OWN PROFILE
router.get('/viewprofile', auth.verify, userController.profileDetails );


router.post('/forgotPassword', userController.forgotPassword)

router.post("/timeInOut", userController.userAttendance)

router.get("/dailyTimeRecord", userController.dailyTimeRecord)

router.patch("/breakInOut", userController.breakTime)



router.patch('/updateRole/:employeeId', auth.verify, userController.updateRole)

router.patch('/resetPassword/:employeeId', auth.verify, userController.resetPassword)

router.patch('/changePassword/:email', userController.changePassword)






module.exports = router;
