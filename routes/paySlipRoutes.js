const express = require('express');
const router = express.Router();
const paySlipController = require("../controllers/paySlipControllers");
const { route } = require('./userRoutes');
const auth = require ('../auth.js');



router.post('/generate', paySlipController.generatePaySlip);

router.post('/generateall', paySlipController.generatePaySlipAllActiveEmployee);

router.get('/', auth.verify, paySlipController.getAllPaySlips)

router.get('/find', auth.verify, paySlipController.getPaySlipById)






module.exports = router