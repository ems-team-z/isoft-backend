const express = require('express');
const router = express.Router();
const auth = require('../auth');
const departmentController = require('../controllers/departmentControllers');

// DEPARTMENT ENDPOINTS HERE
//ENDPOINT GET ALL DEPARTMENT
router.get('/', auth.verify, departmentController.getAllDepartments);

//ENDPOINT CREATE DEPARTMENT
router.post('/create', auth.verify, departmentController.createDepartment);

// ENDPOINT CREATE JOBPOSITION IN DEPARTMENT
router.patch('/addjobs', auth.verify, departmentController.addJobPositionToDeparment);

//ENDPOINT UPDATE DEPARTMENT
router.put('/update',auth.verify, departmentController.updateDepartment);



// JOBPOSITION ENDPOINTS HERE
//ENDPOINT GET ALL JOB POSITIONS 
router.get('/jobpositions',auth.verify, departmentController.getAllJobPositions);

//ENDPOINT CREATE NEW JOB POSITION
router.post('/jobpositions/create',auth.verify, departmentController.createJobPosition);

//ENDPOINT UPDATE DEPARTMENT
router.put('/jobpositions/update/:jobPositionId',auth.verify, departmentController.updateJobPosition);



// INCENTIVES ENDPOINTS HERE
//ENDPOINT GET ALL INCENTIVES
router.get('/incentives',auth.verify, departmentController.getAllIncentives);

//ENDPOINT CREATE NEW INCENTIVE
router.post('/incentives/create',auth.verify, departmentController.createIncentive);

//ENDPOINT UPDATE DEPARTMENT
router.put('/incentives/update',auth.verify, departmentController.updateIncentive);


// DEDUCTION ENDPOINTS HERE
//ENDPOINT GET ALL DEDUCTIONS
router.get('/deductions',auth.verify, departmentController.getAllDeductions);

//ENDPOINT CREATE NEW DEDUCTION
router.post('/deductions/create',auth.verify, departmentController.createDeduction);

//ENDPOINT UPDATE DEDUCTION
router.put('/deductions/update',auth.verify, departmentController.updateDeduction);


// router.get('/viewpayslip', auth.verify, userController.viewPaySlip );

// router.get('/viewprofile', auth.verify, userController.profileDetails );

// router.patch('/updaterole', auth.verify, userController.updateRole );




module.exports = router;